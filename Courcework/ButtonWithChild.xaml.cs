﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Courcework
{
    /// <summary>
    /// Interaction logic for ButtonWithChild.xaml
    /// </summary>
    public partial class ButtonWithChild : UserControl
    {
        public ImageSource Image { get; set; }
        public string Title { get; set; }
        public IEnumerable<string> Items { get; set; }

        public ButtonWithChild()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (img.Visibility == Visibility.Visible && listbox.Visibility == Visibility.Visible)
            {
                img.Visibility = Visibility.Collapsed;
                listbox.Visibility = Visibility.Collapsed;
            }
            else
            {
                img.Visibility = Visibility.Visible;
                listbox.Visibility = Visibility.Visible;
            }
        }
    }
}